package com.daksh.moviescatalog.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.daksh.moviescatalog.model.CatalogItem;
import com.daksh.moviescatalog.model.Movie;
import com.daksh.moviescatalog.model.Rating;
import com.daksh.moviescatalog.model.UserRating;
import com.daksh.moviescatalog.service.MovieInfoService;
import com.daksh.moviescatalog.service.UserRatingService;

@RestController
@RequestMapping("/catalog")
public class MovieCatalogController {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	WebClient.Builder webClientBuilder;

	@Autowired
	MovieInfoService movieInfoService;

	@Autowired
	UserRatingService userRatingService;

	@RequestMapping("/{userId}")
	public List<CatalogItem> getCatolog(@PathVariable("userId") String userId) {

		// make such call to another service
		UserRating userRating = userRatingService.getUserRatings(userId);
		return userRating.getRatings().stream()
				.map(rating -> movieInfoService.getCatalogItem(rating))
				.collect(Collectors.toList());

	}

	@RequestMapping("/async/{userId}")
	public List<CatalogItem> getCatologAsynch(@PathVariable("userId") String userId) {

		// hard coded but you can call service here too same below
		List<Rating> ratings = Arrays.asList(new Rating("1234", 4), new Rating("4567", 3));

		return ratings.stream().map(rating -> {
			Movie movie = webClientBuilder.build().get().uri("http://localhost:8082/movies/" + rating.getMovieId())
					.retrieve().bodyToMono(Movie.class).block();
			return new CatalogItem(movie.getName(), "Test", rating.getRating());
		}).collect(Collectors.toList());

	}
}
