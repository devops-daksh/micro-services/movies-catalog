package com.daksh.moviescatalog.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.daksh.moviescatalog.model.Rating;
import com.daksh.moviescatalog.model.UserRating;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@Service
public class UserRatingService {

	@Autowired
	RestTemplate restTemplate;

	@HystrixCommand(fallbackMethod = "getFallbackUserRatings",
			commandProperties = {
			        @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000"),
			        @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),
			        @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
			        @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "6000"),
			        @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "18000") 
			}, 
			threadPoolKey = "userRatingPool",
			threadPoolProperties = {
					@HystrixProperty(name = "coreSize", value = "30"),
					@HystrixProperty(name = "maxQueueSize", value = "10"),
			        @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "18000") 
			})
	public UserRating getUserRatings(String userId) {
		UserRating userRating = restTemplate.getForObject("http://ratings-service/ratings/users/" + userId,
				UserRating.class);
		return userRating;
	}

	public UserRating getFallbackUserRatings(String userId) {
		UserRating userRating = new UserRating();
		userRating.setUserId(userId);
		userRating.setRatings(Arrays.asList(new Rating("0", 0)));
		return userRating;
	}
}
