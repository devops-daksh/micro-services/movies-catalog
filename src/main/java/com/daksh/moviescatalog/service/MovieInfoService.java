package com.daksh.moviescatalog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.daksh.moviescatalog.model.CatalogItem;
import com.daksh.moviescatalog.model.Movie;
import com.daksh.moviescatalog.model.Rating;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@Service
public class MovieInfoService {

	@Autowired
	RestTemplate restTemplate;
	
	@HystrixCommand(fallbackMethod = "getFallbackCatalogItem",
			commandProperties = {
			        @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000"),
			        @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),
			        @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
			        @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "6000"),
			        @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "18000") 
			}, 
			threadPoolKey = "movieInfoPool",
			threadPoolProperties = {
					@HystrixProperty(name = "coreSize", value = "30"),
					@HystrixProperty(name = "maxQueueSize", value = "10"),
			        @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "18000") 
			})
	public CatalogItem getCatalogItem(Rating rating) {
		Movie movie = restTemplate.getForObject("http://movie-info-service/movies/api/" + rating.getMovieId(), Movie.class);
		return new CatalogItem(movie.getName(), movie.getDescription(), rating.getRating());
	}

	private CatalogItem getFallbackCatalogItem(Rating rating) {
		return new CatalogItem("no movie info available", "Test", rating.getRating());
	}
}
